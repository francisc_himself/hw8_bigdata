#!/usr/bin/python

from PIL import Image, ImageDraw
import argparse
import sys
from pprint import pprint
from random import randint
from enum import Enum
from collections import OrderedDict
from math import sqrt

class nodeType(Enum):
    CORE      = 0
    BORDER    = 1
    NOISE     = 2
    UNCERTAIN = 3

class Point:
    id = 0
    x  = 0
    y  = 0
    isVisited = False
    type = nodeType.UNCERTAIN
    label = 999

colorsList = [
(255, 0, 0),#BLUE
(50, 255, 50),#GREEN
(0, 0, 255),#RED
(255, 0, 255),#MAGENTA
(247, 131, 30),#DARK TURQOUISE
(0, 242, 255),#YELLOW
(98, 156, 38),#DARK GREEN
]

numColors = len(colorsList)

imageCircles = Image.open("dataset/circles.png")
imageFull    = Image.open("dataset/full.png")
imageMoons   = Image.open("dataset/moons.png")
imageSpots   = Image.open("dataset/spots.png")
imageStripes = Image.open("dataset/stripes.png")
imageMyTest  = Image.open("dataset/mytest.png")
imageMyTest1 = Image.open("dataset/mytest1.png")
imagesList = [imageCircles, imageFull, imageMoons, imageSpots, imageStripes]

############## GLOBALS ###########################
processedImage = imageMoons
imageResultColoured = Image.new('RGB', processedImage.size, (255, 255, 255))
processedWidth, processedHeight = processedImage.size

gridSize = 60
############## GLOBALS ###########################

def getGridDims(gridSize):
    return (1+processedWidth/gridSize, 1+processedHeight/gridSize)

gridWidth, gridHeight = getGridDims(gridSize)

coordDict = OrderedDict()
###########################################################################
def makeGrids(gridSize):
    gridDict = OrderedDict()
    gridCurrent = 0
    for idx_i in range(0, processedWidth, gridSize):
        for idx_j in range(0, processedHeight, gridSize):
            print idx_i/gridSize, idx_j/gridSize, ":",
            cornerStart_x, cornerStart_y, cornerEnd_x, cornerEnd_y = \
            idx_i, idx_j, idx_i + gridSize, idx_j + gridSize

            if idx_i + gridSize > processedWidth:
                cornerEnd_x = processedWidth

            if idx_j + gridSize > processedHeight:
                cornerEnd_y = processedHeight

            coordDict[(idx_i/gridSize, idx_j/gridSize)] = (cornerStart_x, cornerStart_y, cornerEnd_x, cornerEnd_y)

            gridDict[(cornerStart_x, cornerStart_y, cornerEnd_x, cornerEnd_y)] = []
            print cornerStart_x, cornerStart_y, cornerEnd_x, cornerEnd_y
            gridCurrent += 1
    return  gridDict
###########################################################################



##################################################
def imgToDict(image):
    dictAllPoints = dict()
    width, height = image.size
    id = 0
    for idx_i in range(width):
        for idx_j in range(height):
            if (image.getpixel((idx_i, idx_j)) == 0):
                # Only black pixels here
                id += 1
                apoint = Point()
                apoint.id = id - 1
                apoint.x = idx_i
                apoint.y = idx_j
                apoint.isVisited = False
                apoint.type = nodeType.UNCERTAIN
                dictAllPoints[(idx_i, idx_j)] = apoint
    return dictAllPoints
##################################################
dataSet = imgToDict(processedImage)

startingLabel = -1

gridsDict = makeGrids(gridSize)

for a_grid in gridsDict:
    for idx_i in range(a_grid[0], a_grid[2]):
        for idx_j in range(a_grid[1], a_grid[3]):
            if processedImage.getpixel((idx_i, idx_j)) == 0:
                gridsDict[a_grid].append(dataSet[idx_i, idx_j])

def DBSCAN(startingLabel, dataSet, Eps, MinPts, gridsDict):
    ClusterLabel = startingLabel
    # For each point P in dataset D
    for idx_tuple, a_coord_tuple in enumerate(dataSet):
        # print "Coords:", dataSet[a_coord_tuple].x, dataSet[a_coord_tuple].y
        if idx_tuple < 1:
            # if P is visited, continue next point
            if dataSet[a_coord_tuple].isVisited == True:
                continue
            # Mark P as visited
            dataSet[a_coord_tuple].isVisited = True
            print "CALLING REGION QUERY ON:", a_coord_tuple
            NeighbourPts = regionQuery(dataSet[a_coord_tuple], Eps)
            if len(NeighbourPts) < MinPts:
                dataSet[a_coord_tuple].type = nodeType.NOISE
                print ("NOISE!")
            else:
                ClusterLabel = 1 + ClusterLabel
                print ("")
                expandCluster(dataSet[a_coord_tuple], NeighbourPts, ClusterLabel, Eps, MinPts, dataSet)
            drawImage()
    print ("Got to label:", ClusterLabel)

def expandCluster(Point, NeighbourPts, Cluster, Eps, MinPts, dataSet):
    # NeighbourPtsCpy = dict(NeighbourPts)
    # AdditionalDiscDict = dict()
    print ("[1] I know this many neighbours:", len(NeighbourPts))

    # pprint(NeighbourPts)


    # add P to cluster C
    dataSet[(Point.x, Point.y)].label = Cluster
    # for each point P' in NeighbourPts
    for a_neigh_pt in NeighbourPts:
        # if not visited
        if dataSet[(a_neigh_pt.x, a_neigh_pt.y)].isVisited == False:
            # mark as visited
            dataSet[(a_neigh_pt.x, a_neigh_pt.y)].isVisited = True
            # NeighbourPts' = regionQuery(P', Eps)
            print "CALLING REGION QUERY ON:", "(", a_neigh_pt.x, a_neigh_pt.y, ")"
            NewNeighbourPts = regionQuery(a_neigh_pt, Eps)
            if len(NewNeighbourPts) >= MinPts:
                oldLen = len(NeighbourPts)
                NeighbourPts += NewNeighbourPts
                print "Just extended from", oldLen, len(NeighbourPts)
        if dataSet[(a_neigh_pt.x, a_neigh_pt.y)].label == 999:
            dataSet[(a_neigh_pt.x, a_neigh_pt.y)].label = Cluster


    print ("[2] I know this many neighbours:", len(NeighbourPts))

def regionQuery(Point, Eps):
    retList = []

    print "REGION QUERY:", len(gridsDict), gridWidth, "x", gridHeight
    for idx_grid, a_grid in enumerate(gridsDict.keys()):
        # print "GRID#", idx_grid
        x_st_gr, y_st_gr, x_end_gr, y_end_gr = a_grid[0], a_grid[1], \
                a_grid[2], a_grid[3]

        if Point.x > x_st_gr and Point.x < x_end_gr and \
           Point.y > y_st_gr and Point.y < y_end_gr:
            # now only get points in neighbour grid tiles
                print "Found it here:", idx_grid, "->",
                x_found, y_found = idx_grid / gridWidth, idx_grid % gridWidth
                print x_found, y_found
                break

    # look in neighbouring tiles:
    if 0 <= x_found - 1 and 0 <= y_found - 1:
        print "Hit: 1"
        # pprint(coordDict)
        retList += getGoodDistance(Point, gridsDict[coordDict[(x_found, y_found)]], Eps)
        pass

    if 0 <= x_found - 1:
        print "Hit: 2"
        retList += getGoodDistance(Point, gridsDict[coordDict[(x_found, y_found)]], Eps)
        pass

    if 0 <= x_found - 1 and y_found + 1 < gridHeight:
        print "Hit: 3"
        retList += getGoodDistance(Point, gridsDict[coordDict[(x_found, y_found)]], Eps)
        pass

    if 0 <= y_found - 1:
        print "Hit: 4"
        retList += getGoodDistance(Point, gridsDict[coordDict[(x_found, y_found)]], Eps)
        pass

    if y_found + 1 < gridHeight:
        print "Hit: 5"
        retList += getGoodDistance(Point, gridsDict[coordDict[(x_found, y_found)]], Eps)
        pass

    if x_found + 1 < gridWidth and y_found - 1 >= 0:
        print "Hit: 6"
        retList += getGoodDistance(Point, gridsDict[coordDict[(x_found, y_found)]], Eps)
        pass

    if x_found + 1 < gridWidth:
        print "Hit: 7"
        retList += getGoodDistance(Point, gridsDict[coordDict[(x_found, y_found)]], Eps)
        pass

    if x_found + 1 < gridWidth and y_found + 1 < gridHeight:
        print "Hit: 8"
        retList += getGoodDistance(Point, gridsDict[coordDict[(x_found, y_found)]], Eps)
        pass






    # print "RETLIST LEN=", len(retList)

    draw = ImageDraw.Draw(imageResultColoured)

    for apoint in retList:
        draw.rectangle((apoint.x-3, apoint.y-3, \
                        apoint.x+3, apoint.y+3), \
                       fill='yellow', outline='black')


    return retList

def getGoodDistance(Point, Dataset, Eps):
    retList = []

    x_point = Point.x
    y_point = Point.y

    for idx, a_point in enumerate(Dataset):
        x_apoint = a_point.x
        y_apoint = a_point.y

        distance = sqrt(pow(x_point-x_apoint, 2)+pow(y_point-y_apoint, 2))

        if distance <= Eps:
            retList.append(dataSet[(x_point, y_point)])
    return  retList





def drawImage():
    for idx_i in range(processedWidth):
        for (idx_j) in range(processedHeight):
            if processedImage.getpixel((idx_i, idx_j))==0:
                if dataSet[(idx_i, idx_j)].label != 999:
                    imageResultColoured.putpixel((idx_i, idx_j), colorsList[dataSet[(idx_i, idx_j)].label%numColors])
    imageResultColoured.show()




######## Main ###########

#
DBSCAN(startingLabel, dataSet, 100, 1, gridsDict)
#
#
#
draw = ImageDraw.Draw(imageResultColoured)

# pprint(gridsDict)

for a_grid in gridsDict:
    draw.rectangle((a_grid[0], a_grid[1], \
                  a_grid[2], a_grid[3]), \
                  outline='black')

# pprint(gridsDict)

imageResultColoured.show()
# pprint(gridsDict)












